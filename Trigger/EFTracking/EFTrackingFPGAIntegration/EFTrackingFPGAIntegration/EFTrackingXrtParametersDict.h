/*
 *   Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
 */

#ifndef EFTRACKING_XRT_PARAMETERS_DICT
#define EFTRACKING_XRT_PARAMETERS_DICT

#include "EFTrackingFPGAIntegration/EFTrackingXrtParameters.h"

#endif

