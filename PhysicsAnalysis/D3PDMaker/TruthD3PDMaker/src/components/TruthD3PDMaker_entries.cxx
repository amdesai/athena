// Associator tool(s):
#include "../TruthParticleChildAssociationTool.h"
#include "../TruthParticleParentAssociationTool.h"
#include "../TruthParticleProdVertexAssociationTool.h"
// Filler tool(s):
#include "../TruthParticleBremFillerTool.h"
#include "../TruthParticleFillerTool.h"
#include "../TruthParticleClassificationFillerTool.h"
#include "../D3PDMCTruthClassifier.h"
// Filter tool(s):

// Associator tool(s):
DECLARE_COMPONENT( D3PD::TruthParticleChildAssociationTool )
DECLARE_COMPONENT( D3PD::TruthParticleParentAssociationTool )
DECLARE_COMPONENT( D3PD::TruthParticleProdVertexAssociationTool )
// Filler tool(s):
DECLARE_COMPONENT( D3PD::TruthParticleBremFillerTool )
DECLARE_COMPONENT( D3PD::TruthParticleFillerTool )
DECLARE_COMPONENT( D3PD::TruthParticleClassificationFillerTool )
DECLARE_COMPONENT( D3PD::D3PDMCTruthClassifier )

