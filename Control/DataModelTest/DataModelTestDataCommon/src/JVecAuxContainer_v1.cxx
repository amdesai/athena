/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file DataModelTestDataCommon/src/JVecAuxContainer_c1.cxx
 * @author scott snyder <snyder@bnl.gov>
 * @date May, 2024
 * @brief For testing jagged vectors.
 */


#include "DataModelTestDataCommon/versions/JVecAuxContainer_v1.h"


namespace DMTest {


JVecAuxContainer_v1::JVecAuxContainer_v1()
  : xAOD::AuxContainerBase()
{
}


} // namespace DMTest
